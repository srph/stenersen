@extends('template.master')

@section('title') - {{ $main->title }} @stop

@section('content')
	<div class="row">
		<div class="col-md-12">
			@if($main->isHome)
				<div class="cover">
					<img src="{{ url('themes/main/img/cover.png') }}">

					<div class="text">
						<h2 class="text-center">
							Welcome to <strong> {{ Setting::title() }} </strong>
						</h2>

						<h4 class="text-center">
							{{ Setting::frontDescription() }}
						</h4>

						<br />
						<div class="text-center">
							<button type="button" class="btn btn-success">
								Read More
							</button>
						</div>
					</div>
				</div>
			@else
			
				<div class="page-cover img-rounded">
					<img src="{{ url('themes/main/img/cover.png') }}">
				</div>
			
				<div class="page-text">
					<h2 class="text-center">
						{{ $main->title }}
					</h2>
				</div>
			@endif
		</div>
	</div>

	<br />

	<div class="panel panel-default">
		<div class="panel-body">
			<div class="panel-narrow">
				@if(Session::has('success'))
					@include('template/modules/alert.success')
				@else
					<iframe src="{{ url('dashboard/sandbox/' . $main->slug) }}" height="100%" width="100%" frameBorder="0"></iframe>
				@endif
			</div>
		</div>
	</div>
@stop

@section('scripts')
	<script>
		function createError(message) {
			return '<div class="alert alert-danger error">' +
				message +
				'</div>';
		}

		$('button#submit-button').on('click', function(e) {
			e.preventDefault();
			var $e;
			if(($e = $('.error')).length !== 0)
				$e.remove();
			var message = $("textarea[name=message]").val().replace(/\n|\r/g, "&#10;");
			console.log(message);
			$.ajax({
				url: '{{ url('mail/us') }}' + '?message='  + message,
				data: {
					email: $('[name=email]').val(),
					full_name: $('[name=full_name]').val(),
					subject: $('[name=subject]').val()
				},
				type: 'POST',
				dataType: 'json',
				success: function(data) {
					var error;
					if(data.status) {
						location.reload();
					} else {
						$('form#custom-form').prepend(createError(data.error));

						// Title
						if(data.errors.subject) {
							error = createError(data.errors.subject);
							$('#subject').append(error);
							error = undefined;
						}
						// Email
						if(data.errors.email) {
							error = createError(data.errors.email);
							$('#email').append(error);
							error = undefined;
						}

						// Full Name
						if(data.errors.full_name) {
							error = createError(data.errors.full_name);
							$('#full_name').append(error);
							error = undefined;
						}

						// Message
						if(data.errors.message) {
							error = createError(data.errors.message);
							$('#message').append(error);
							error = undefined;
						}
					}
				}
			});
		});
	</script>
@stop