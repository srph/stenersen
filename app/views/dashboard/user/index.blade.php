@extends('template.dashboard')

@section('title') - Behandle kontoer @stop

@section('content')
	<div class="page-header">
		<h1>
			Behandle kontoer
		</h1>
	</div><!-- /.page-header -->

	<div class="row">
		<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
			<div class="row">
				<div class="col-xs-12">
					<div class="table-responsive">
						<table id="sample-table-1" class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th>#</th>
									<th>Email</th>
									<th>Aktivert</th>
									<th>Fornavn</th>
									<th>Etternavn</th>
									<th>Handlinger</th>
								</tr>
							</thead>

							<tbody>
								@foreach($users as $user)
									<tr>
										<td> {{ $user->id }} </td>
										<td> {{ $user->email }} </td>
										<td> <span class="label label-sm label-warning">{{ User::activatedString($user) }}</span> </td>
										<td> {{ $user->first_name }} </td>
										<td> {{ $user->last_name }} </td>
										<td>
											<div class="visible-md visible-lg hidden-sm hidden-xs btn-group">

												<a class="btn btn-xs btn-info" href="{{ URL::to('dashboard/account/' . $user->id . '/edit') }}">
													<i class="icon-edit bigger-120"></i>
												</a>

												<a class="btn btn-xs btn-danger" data-id="{{ $user->id }}" data-toggle="delete" href="#" id="trash">
													<i class="icon-trash bigger-120"></i>
												</a>
											</div>
										</td>
									</tr>	
								@endforeach
							</tbody>
						</table>
					</div><!-- /.table-responsive -->
				</div><!-- /span -->
			</div><!-- /row -->							
		</div><!-- PAGE CONTENT ENDS -->
	</div><!-- /.col -->
</div><!-- /.row -->
@stop

@section('scripts')
	{{ HTML::script('assets/js/bootbox.min.js') }}

	<script>
		$('[data-toggle="delete"]').on('click', function(e) {
			e.preventDefault();
			self = $(this);

			bootbox.confirm("Are you sure to delete this page?", function(res) {
				if(res === true) {
					$.ajax({
						url: '{{ URL::to('dashboard/account') }}/' + self.data('id'),
						type: 'DELETE',
						dataType: 'json',
						success: function(data) {
							if(data.status) {
								location.reload();
							}
						}
					});
				}
			});
		});
	</script>
@stop