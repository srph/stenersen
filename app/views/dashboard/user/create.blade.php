@extends('template.dashboard')

@section('title') - Opprett ny konto @stop

@section('content')
	<div class="page-header">
		<h1> Opprett ny konto </h1>
	</div><!-- /.page-header -->

	<div class="row">
		<div class="col-xs-12">
			{{ Form::open(array('id' => 'form-create', 'class' => 'form-horizontal', 'role' => 'form', 'url' => 'dashboard/account/')) }}
				{{ Form::token() }}
				<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right"> Email </label>
					<div class="col-sm-9">
						<input type="email" name="email" class="col-xs-10 col-sm-5" id="email">
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right"> Passord </label>
					<div class="col-sm-9">
						<input type="password" name="password" class="col-xs-10 col-sm-5">
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right"> Bekreft passord </label>
					<div class="col-sm-9">
						<input type="password" name="password_confirmation" class="col-xs-10 col-sm-5">
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right"> Fornavn </label>
					<div class="col-sm-9">
						<input type="text" name="first_name" class="col-xs-10 col-sm-5">
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right"> Etternavn </label>
					<div class="col-sm-9">
						<input type="text" name="last_name" class="col-xs-10 col-sm-5">
					</div>
				</div>

				<div class="form-group">
					<small class="col-sm-3 control-label no-padding-right"> Aktivert </small>
					<div class="col-sm-9">
						<label>
							<input name="activated" type="checkbox" class="ace ace-switch ace-switch-2">
							<span class="lbl"></span>
						</label>
					</div>
				</div>

				<div class="col-md-offset-3 col-md-9">
					<button id="submit-btn" type="submit" class="btn btn-info">
						Opprett konto
					</button>
				</div>
			{{ Form::close() }}
		</div>
	</div>
@stop

@section('scripts')
	<script>
		$('#submit-btn').on('click', function(e) {
			e.preventDefault();

			var activated = ($('[name=activated]').is(':checked'))
				? true
				: false;

			$.ajax({
				url: '{{ URL::to('dashboard/account') }}',
				data: {
					email: $('[name=email]').val(),
					password: $('[name=password]').val(),
					password_confirmation: $('[name=password_confirmation]').val(),
					first_name: $('[name=first_name]').val(),
					last_name: $('[name=last_name]').val(),
					activated: activated
				},
				type: 'POST',
				dataType: 'json',
				success: function(data) {
					if( ($e = $('.error')).length !== 0)
						$e.remove();

					if(data.status) {
						window.location = '{{ URL::to('dashboard/account') }}';
					} else {
						var errors = []
						$.each(data.errors, function(i, v) {
							errors[i] = v + '<br />';
						});
						
						var error = '<div class="alert alert-danger" id="error">' +
							data.error + '<br >' +
							errors + '</div>';
						$('#form-create').prepend(error);

						// $('#email').append('<span class="label label-danger">' + errors[0] + '</span>');
						// $('[name=password]').append('<span class="label label-danger">' + data.errors.password + '</span>');
						// $('[name=password_confirmation]').append('<span class="label label-danger">' + data.errors.password_confirmation + '</span>');
						// $('[name=first_name]').append('<span class="label label-danger">' + data.errors.first_name + '</span>');
						// $('[name=last_name]').append('<span class="label label-danger">' + data.errors.last_name + '</span>');
					}
				}
			});
		});
	</script>
@stop