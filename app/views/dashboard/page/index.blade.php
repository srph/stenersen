@extends('template.dashboard')

@section('title') - Administrer Sider @stop

@section('content')
	<div class="page-header">
		<h1>
			Administrer Sider
		</h1>
	</div><!-- /.page-header -->

	<div class="row">
		<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
			<div class="row">
				<div class="col-xs-12">
					<div class="table-responsive">
						<table id="sample-table-1" class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th>Bestill</th>
									<th>Tittel</th>
									<th>Slug</th>
									<th>Status</th>
									<th>i Navigation</th>
									<th>Hjemmeside</th>
									<th>Handlinger</th>
								</tr>
							</thead>

							<tbody>
								@foreach($pages as $page)
									<tr>
										<td> {{ $page->order }} </td>

										<td> {{ $page->title }} </td>
										<td> {{ $page->slug }} </td>
										<td> {{ $page->live() }} </td>
										<td> {{ $page->inNavigation() }} </td>
										<td class="hidden-480">
											<span class="label label-sm label-warning">{{ $page->isHomepage() }}</span>
										</td>

										<td>
											<div class="visible-md visible-lg hidden-sm hidden-xs btn-group">

												<a class="btn btn-xs btn-info" href="{{ URL::to('dashboard/page/' . $page->id . '/edit') }}">
													<i class="icon-edit bigger-120"></i>
												</a>

												<a class="btn btn-xs btn-danger" data-id="{{ $page->id }}" data-toggle="delete" href="#" id="trash">
													<i class="icon-trash bigger-120"></i>
												</a>
											</div>
										</td>
									</tr>	
								@endforeach
							</tbody>
						</table>
					</div><!-- /.table-responsive -->
				</div><!-- /span -->
			</div><!-- /row -->							
		</div><!-- PAGE CONTENT ENDS -->
	</div><!-- /.col -->
</div><!-- /.row -->
@stop

@section('scripts')
	{{ HTML::script('assets/js/bootbox.min.js') }}

	<script>
		$('[data-toggle="delete"]').on('click', function(e) {
			e.preventDefault();
			self = $(this);

			bootbox.confirm("Are you sure to delete this page?", function(res) {
				if(res === true) {
					$.ajax({
						url: 'page/' + self.data('id'),
						type: 'DELETE',
						dataType: 'json',
						success: function(data) {
							if(data.status) {
								location.reload();
							}
						}
					});
				}
			});
		});
	</script>
@stop