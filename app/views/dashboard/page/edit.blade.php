@extends('template.dashboard')

@section('title') - Rediger {{ $page->title }} @stop

@section('content')
<div class="page-header">
						<h1> Rediger {{ $page->title }} </h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								{{ Form::open(array('id' => 'form-create')) }}
									{{ Form::token() }}
									<div class="row">
										<div class="col-xs-6">
											<div id="title" class="form-group">
												<label>
													Tittel
													(<b id="meta-title-length" class="red">{{ strlen($page->title) }}</b>/70)
												</label>
												<input name="title" type="text" class="form-control" value="{{ $page->title }}">
											</div>
										</div>

										<div class="col-xs-6">
											<div id="slug" class="form-group">
												<label> Custom Slug </label>
												<input type="text" name="slug" class="form-control" value="{{ $page->slug }}">
												<p class="help-block"> I nettadressen vil det komme adressen og så vil det være en «/» som spesifiserer hvilken underside du er på. (Eksempel: <em> www.marikova.no/kontakt </em>) </p>
											</div>
										</div>
									</div>

									<h4 class="header green clearfix">
										Innhold
										<span class="block pull-right">
											<small class="grey middle">Velg stil: &nbsp;</small>

											<span class="btn-toolbar inline middle no-margin">
												<span data-toggle="buttons" class="btn-group no-margin">
													<label class="btn btn-sm btn-yellow">
														1
														<input type="radio" value="1" />
													</label>

													<label class="btn btn-sm btn-yellow active">
														2
														<input type="radio" value="2" />
													</label>

													<label class="btn btn-sm btn-yellow">
														3
														<input type="radio" value="3" />
													</label>
												</span>
											</span>
										</span>
									</h4>

									<div class="wysiwyg-editor" id="editor1"></div>

									<script type="text/javascript">
										var $path_assets = "assets";//this will be used in loading jQuery UI if needed!
									</script>

									<div class="space-6"></div>
									<div class="row">

										<div class="col-xs-6">
											<label> Bestill </label>
											<select class="form-control" name="order">
												@for($i = 1; $i <= Page::getLastOrder()->order; $i++)
													<option {{ $page->selected($i) }} value="{{ $i }}"> {{ $i }} {{ $page->checkCurrentOrder($i) }} </option>
												@endfor
											</select>
										</div>
										<div class="col-xs-2">
												<label class="inline">
													<small> Gjør siden live: </small> <br />
													<input name="live" class="ace ace-switch ace-switch-5" {{ $page->checked('live') }} type="checkbox" />
													<span class="lbl"></span>
												</label>
										</div>

										<div class="col-xs-2">
											<label class="inline">
												<small> Vis i navigeringen </small> <br />
												<input name="inNavigation" class="ace ace-switch ace-switch-5" {{ $page->checked('inNavigation') }} type="checkbox" />
												<span class="lbl"></span>
											</label>
										</div>

										<div class="col-xs-2">
											<label class="inline">
												<small> Satt til standard hjemmeside </small>  <br />
												<input name="isHome" class="ace ace-switch ace-switch-5" {{ $page->checked('isHome') }} type="checkbox" />
												<span class="lbl"></span>
											</label>
										</div>
									</div>

									<div class="space-6"></div>
									<div class="row">
										<div class="col-xs-6">
											<div class="form-group">
												<label>
													Meta Beskrivelse
													(<b id="meta-description-length" class="red">{{ strlen($page->metaDescription) }}</b>/152)
												</label>
												<textarea name="metaDescription" class="form-control">{{ $page->metaDescription }}</textarea>
												<p class="help-block"> Meta beskrivelsen er en beskrivelse av siden, som vil bli synlig gjennom søkemotorer. </p>
											</div>
										</div>

										<div class="col-xs-6">
											<div class="form-group">
												<label> SEO Forhåndsvisning: </label>
												<blockquote>
													<p id="meta-title" style="margin-bottom: 0">
														<a href="#" style="color: blue; text-decoration: underline; font-family: Arial">
															{{ Setting::title() }}&nbsp;-&nbsp;{{ $page->title }}
														</a>
													</p>

													<p id="meta-slug" style="color: green; font-size:14px; font-family: Arial; margin-bottom: 0">
														{{ $page->getLink() }}
													</p>

													<p id="meta-description" style="font-family: Arial; font-size: 13px">
														{{ $page->metaDescription }}
													</p>
												</blockquote>
											</div>
										</div>
									</div>

									<br />
									<div class="form-group">									
										<button id='form-submit' type="submit" class="btn btn-success">
											<i class="icon-ok"></i>
											Rediger side
										</button>
									</div>
								{{ Form::close() }}

								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->

					<div class="hide" id="bodieh">
						{{ $page->body }}
					</div>
@stop

@section('page_scripts')
	<!-- page specific plugin scripts -->

	{{ HTML::script('assets/js/jquery-ui-1.10.3.custom.min.js') }}
	{{ HTML::script('assets/js/jquery.ui.touch-punch.min.js') }}
	{{ HTML::script('assets/js/markdown/markdown.min.js') }}
	{{ HTML::script('assets/js/markdown/bootstrap-markdown.min.js') }}
	{{ HTML::script('assets/js/jquery.hotkeys.min.js') }}
	{{ HTML::script('assets/js/bootstrap-wysiwyg.min.js') }}
	{{ HTML::script('assets/js/bootbox.min.js') }}
@stop

@section('inline_scripts')
	<script type="text/javascript">
				jQuery(function($){
		
		function showErrorAlert (reason, detail) {
			var msg='';
			if (reason==='unsupported-file-type') { msg = "Unsupported format " +detail; }
			else {
				console.log("error uploading file", reason, detail);
			}
			$('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>'+ 
			 '<strong>File upload error</strong> '+msg+' </div>').prependTo('#alerts');
		}

		//$('#editor1').ace_wysiwyg();//this will create the default editor will all buttons

		//but we want to change a few buttons colors for the third style
		$('#editor1').ace_wysiwyg({
			toolbar:
			[
				'font',
				null,
				'fontSize',
				null,
				{name:'bold', className:'btn-info'},
				{name:'italic', className:'btn-info'},
				{name:'strikethrough', className:'btn-info'},
				{name:'underline', className:'btn-info'},
				null,
				{name:'insertunorderedlist', className:'btn-success'},
				{name:'insertorderedlist', className:'btn-success'},
				{name:'outdent', className:'btn-purple'},
				{name:'indent', className:'btn-purple'},
				null,
				{name:'justifyleft', className:'btn-primary'},
				{name:'justifycenter', className:'btn-primary'},
				{name:'justifyright', className:'btn-primary'},
				{name:'justifyfull', className:'btn-inverse'},
				null,
				{name:'createLink', className:'btn-pink'},
				{name:'unlink', className:'btn-pink'},
				null,
				{name:'insertImage', className:'btn-success'},
				null,
				'foreColor',
				null,
				{name:'undo', className:'btn-grey'},
				{name:'redo', className:'btn-grey'}
			],
			'wysiwyg': {
				fileUploadError: showErrorAlert
			}
		}).prev().addClass('wysiwyg-style2');

		

		$('#editor2').css({'height':'200px'}).ace_wysiwyg({
			toolbar_place: function(toolbar) {
				return $(this).closest('.widget-box').find('.widget-header').prepend(toolbar).children(0).addClass('inline');
			},
			toolbar:
			[
				'bold',
				{name:'italic' , title:'Change Title!', icon: 'icon-leaf'},
				'strikethrough',
				null,
				'insertunorderedlist',
				'insertorderedlist',
				null,
				'justifyleft',
				'justifycenter',
				'justifyright'
			],
			speech_button:false
		});


		$('[data-toggle="buttons"] .btn').on('click', function(e){
			var target = $(this).find('input[type=radio]');
			var which = parseInt(target.val());
			var toolbar = $('#editor1').prev().get(0);
			if(which == 1 || which == 2 || which == 3) {
				toolbar.className = toolbar.className.replace(/wysiwyg\-style(1|2)/g , '');
				if(which == 1) $(toolbar).addClass('wysiwyg-style1');
				else if(which == 2) $(toolbar).addClass('wysiwyg-style2');
			}
		});


		

		//Add Image Resize Functionality to Chrome and Safari
		//webkit browsers don't have image resize functionality when content is editable
		//so let's add something using jQuery UI resizable
		//another option would be opening a dialog for user to enter dimensions.
		if ( typeof jQuery.ui !== 'undefined' && /applewebkit/.test(navigator.userAgent.toLowerCase()) ) {
			
			var lastResizableImg = null;
			function destroyResizable() {
				if(lastResizableImg == null) return;
				lastResizableImg.resizable( "destroy" );
				lastResizableImg.removeData('resizable');
				lastResizableImg = null;
			}

			var enableImageResize = function() {
				$('.wysiwyg-editor')
				.on('mousedown', function(e) {
					var target = $(e.target);
					if( e.target instanceof HTMLImageElement ) {
						if( !target.data('resizable') ) {
							target.resizable({
								aspectRatio: e.target.width / e.target.height,
							});
							target.data('resizable', true);
							
							if( lastResizableImg != null ) {//disable previous resizable image
								lastResizableImg.resizable( "destroy" );
								lastResizableImg.removeData('resizable');
							}
							lastResizableImg = target;
						}
					}
				})
				.on('click', function(e) {
					if( lastResizableImg != null && !(e.target instanceof HTMLImageElement) ) {
						destroyResizable();
					}
				})
				.on('keydown', function() {
					destroyResizable();
				});
		    }
			
			enableImageResize();

			/**
			//or we can load the jQuery UI dynamically only if needed
			if (typeof jQuery.ui !== 'undefined') enableImageResize();
			else {//load jQuery UI if not loaded
				$.getScript($path_assets+"/js/jquery-ui-1.10.3.custom.min.js", function(data, textStatus, jqxhr) {
					if('ontouchend' in document) {//also load touch-punch for touch devices
						$.getScript($path_assets+"/js/jquery.ui.touch-punch.min.js", function(data, textStatus, jqxhr) {
							enableImageResize();
						});
					} else	enableImageResize();
				});
			}
			*/
		}
	});
			</script>
@stop

@section('scripts')
	<script>
		function createError(message) {
			return '<div class="alert alert-danger error">' +
				message +
				'</div>';
		}

		$('#editor1').html($('#bodieh').html());
		var baseTitle = '{{ Setting::title() }} - ';
		var baseURL = '{{ url() }}/';

		var title = $('[name=title]');
		var slug = $('[name=slug]');
		var metaDesc = $('[name=metaDescription]');

		title.on('click', function() {
			var self = $(this);
			self.keyup(function() {
				var length = self.val().length;
				var limit = 70;
				$('p#meta-title a').text(baseTitle + self.val());
				$('b#meta-title-length').text(length);
				if(length > limit) {
					self.val(self.val().toString().substring(0, limit))
				}
			})
		});

		slug.on('click', function() {
			var self = $(this);
			self.keyup(function() {
				$('p#meta-slug').text(baseURL + self.val());
			})
		});

		metaDesc.on('click', function() {
			var self = $(this);
			self.keyup(function(e) {
				var length = self.val().length;
				var limit = 152;
				$('p#meta-description').text(self.val());
				$('b#meta-description-length').text(length);
				if(length > limit) {
					self.val(self.val().toString().substring(0, limit))
				}
			});
		});

		$('#form-submit').on('click', function(e) {
			e.preventDefault();

			var isHome = ($('[name=isHome]').is(':checked'))
				? true
				: false;

			var live = ($('[name=live]').is(':checked'))
				? true
				: false;

			var inNavigation = ($('[name=inNavigation]').is(':checked'))
				? true
				: false;

			$.ajax({
				url: '{{ url('dashboard/page') }}' + '/' + '{{ $page->id }}',
				type: 'PUT',
				data: '&title=' + title.val() +
					'&slug=' + slug.val() +
					'&live=' + live +
					'&isHome=' + isHome +
					'&inNavigation=' + inNavigation +
					'&order=' + $('[name=order]').val() +
					'&body=' + $('#editor1').html() +
					'&metaDescription=' + metaDesc.val(),
				dataType: 'json',
				success: function(data) {
					console.log(data);
					// Remove the error
					var $e;
					if(($e = $('.error')).length >= 1)
						$e.remove();

					if(!data.status) {
						var error;
						$('#form-create').prepend(createError(data.error));

						if(data.errors.slug) {
							console.log('oyyy');
							error = createError(data.errors.slug);
							$('#slug').append(error);
							error = undefined;
						}
					} else {
						window.location = '{{ URL::route('dashboard.page.index') }}';
					}
				}
			});
		});
	</script>
@stop