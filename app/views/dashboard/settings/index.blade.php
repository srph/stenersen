@extends('template.dashboard')

@section('title') - Konfigurasjoner @stop

@section('content')
	<div class="page-header">
		<h1>
			Konfigurasjoner
		</h1>
	</div><!-- /.page-header -->

	<div class="row">
		<div class="col-xs-12">
			<!-- PAGE CONTENT BEGINS -->
			{{ Form::open(array('method' => 'PUT')) }}
				{{ Form::token() }}
				<div class="row">
					<div class="col-xs-6">
						<div class="form-group">
							<label> Nettstedet Tittel </label>
							<input type="text" class="form-control" name="title" value="{{ Setting::title() }}" />
							<p class="help-block">
								Nettstedets standard som vil bli brukt i hele området
							</p>
						</div>
					</div>

					<div class="col-xs-6">
						<div class="form-group">
							<label> Slagord </label>
							<input type="text" class="form-control" name="slogan" value="{{ Setting::slogan() }}" />
						</div>
					</div>
				</div>

				<div class="space-6"></div>
				<div class="row">
					<div class="col-xs-6">
						<div class="form-group">
							<label> Email </label>
							<input type="email" class="form-control" name="email" value="{{ Setting::email() }}">
							<p class="help-block">
								Standard e-postadresse som benyttes i kontaktskjemaet
							</p>
						</div>
					</div>

					<div class="col-xs-6">
						<div class="form-group">
							<label> Bunntekst </label>
							<input type="text" class="form-control" name="footer" value="{{ Setting::footer() }}">
							<p class="help-block">
								Denne meldingen vil bli kølle til bunnteksten note.
							</p>
						</div>
					</div>
				</div>

				<div class="space-6"></div>
				<div class="row">
					<div class="col-xs-6">
						<label> Hjemmeside Beskrivelse </label>
						<input type="text" class="form-control" name="frontDescription" value="{{ Setting::frontDescription() }}">
						<p class="help-block"> Beskrivelse vises på hjemmesiden </p>
					</div>

					<div class="col-xs-6">
						<label class="inline">
							<small> Vis på hjemmesiden </small> <br />
							<input name="liveSite" class="ace ace-switch ace-switch-5" {{ Setting::checked('liveSite') }} type="checkbox" />
							<span class="lbl"></span>
						</label>
					</div>
				</div>

				<div class="space-6"></div>
				<div class="form-group">
					{{ Form::submit('Konfigurasjoner', array('class' => 'btn btn-success')) }}
				</div>
			{{ Form::close() }}
			<!-- PAGE CONTENT ENDS -->
		</div><!-- /.col -->
	</div><!-- /.row -->
@stop