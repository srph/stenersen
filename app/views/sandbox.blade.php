<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<!-- Loading Bootstrap -->
		<link href="http://fonts.googleapis.com/css?family=Leckerli+One" rel="stylesheet" type="text/css">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,700,600,300' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
		<link href="http://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet" type="text/css">
		{{ HTML::style('assets/css/bootstrap.min.css') }}
		{{ HTML::style('themes/main/css/stylesheet.css') }}
	</head>

	<body>
		{{ $main->convertedModules() }}
		<script>
			function createError(message) {
				return '<div class="alert alert-danger error">' +
					message +
					'</div>';
			}

			$('button#submit-button').on('click', function(e) {
				e.preventDefault();
				var $e;
				if(($e = $('.error')).length !== 0)
					$e.remove();
				var message = $("textarea[name=message]").val().replace(/\n|\r/g, "&#10;");
				console.log(message);
				$.ajax({
					url: '{{ url('mail/us') }}' + '?message='  + message,
					data: {
						email: $('[name=email]').val(),
						full_name: $('[name=full_name]').val(),
						subject: $('[name=subject]').val()
					},
					type: 'POST',
					dataType: 'json',
					success: function(data) {
						var error;
						if(data.status) {
							location.reload();
						} else {
							$('form#custom-form').prepend(createError(data.error));

							// Title
							if(data.errors.subject) {
								error = createError(data.errors.subject);
								$('#subject').append(error);
								error = undefined;
							}
							// Email
							if(data.errors.email) {
								error = createError(data.errors.email);
								$('#email').append(error);
								error = undefined;
							}

							// Full Name
							if(data.errors.full_name) {
								error = createError(data.errors.full_name);
								$('#full_name').append(error);
								error = undefined;
							}

							// Message
							if(data.errors.message) {
								error = createError(data.errors.message);
								$('#message').append(error);
								error = undefined;
							}
						}
					}
				});
			});
		</script>
	</body>
</html>