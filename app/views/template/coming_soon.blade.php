<!DOCTYPE html>
<html>
<head>
	<title> {{ Setting::title() }} - Coming Soon! </title>
	{{ HTML::style('assets/css/bootstrap.min.css') }}
	{{ HTML::style('assets/css/comingsoon.css') }}
</head>

<body>
	<div class="container">
		<h2> Coming Soon </h2>
		<form>
			<div id="tweet_content">
				<p> Tweet to Join! </p>
				<a id="tweet-share" href="https://twitter.com/share" type="button" class="twitter-share-button" data-via="stenersen">
				</a>
			</div>


			<div id="email-feed" class="hide">
				{{ Form::open() }}
					{{ Form::token() }}
					<div class="form-group">
						<label> Signup for early access to beta: </label>
						<input type="email" name="email" class="form-control">
					</div>

					<div class="form-group">
						<button id="email-submit" type="submit" class="btn btn-success">Sign up!</button>
					</div>
				{{ Form::close() }}
			</div>
	</div>

	{{ HTML::script('assets/js/jquery-2.0.3.min.js') }}
	{{ HTML::script('https://platform.twitter.com/widgets.js') }}
	<script>
		twttr.ready(function(twttr) {
			twttr.events.bind('tweet', function(event) {
				if(!event) return;
				$('#tweet_content').addClass('hide');
				$('#email-feed').removeClass('hide');
			});
		});
	</script>
</body>
</html>