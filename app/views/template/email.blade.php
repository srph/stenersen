<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	{{ HTML::style('email/email.css') }}
</head>

<body>
	<table class="body-wrap">
		<tbody>
			<tr>
				<td></td>
				<td class="container" bg-color="#FFFFFF">
					<div class="content">
						<table>
							<tbody>
								<tr>
									<td>
										{{{ $msg }}}
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</td>
		</tbody>
	</table>
</body>
</html>