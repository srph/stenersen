<!DOCTYPE html>
<html lang="en">
	<head>
		<title> {{ Setting::title() }} @yield('title') </title>
		<meta charset="utf-8" />
		<meta name="title" content="{{ $main->metaTitle() }}" />
		<meta name="description" content="{{ $main->metaDescription() }}" />
		<meta name="author" content="{{ Setting::title() }}" />
		<meta name="viewport" content="width=1000, initial-scale=1.0, maximum-scale=1.0">

		<!-- Loading Bootstrap -->
		<link href="http://fonts.googleapis.com/css?family=Leckerli+One" rel="stylesheet" type="text/css">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,700,600,300' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
		<link href="http://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet" type="text/css">
		{{ HTML::style('assets/css/bootstrap.min.css') }}
		{{ HTML::style('themes/main/css/stylesheet.css') }}
		@yield('styles')
	</head>

	<body>
		<div class="container">
			<nav class="navbar navbar-default" role="navigation">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-01">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">{{ Setting::title() }}</a>
				</div>

				<div class="collapse navbar-collapse" id="navbar-collapse-01">
					<ul class="nav navbar-nav navbar-right">					 
						@foreach($pages as $page)
						<li class="@if(Request::is($page->slug) || ($page->isHome && Request::is('/')))active@endif">
							<a href="{{ $page->getLink() }}">{{ $page->title }} </a>
						</li>
						@endforeach
					 </ul>					
				</div><!-- /.navbar-collapse -->
			</nav><!-- /navbar -->
		
			<div class="row">
				<div class="col-md-12">
					@yield('content')
				</div>
			</div>

			<footer class="navbar navbar-default" role="footer">
				<p class="navbar-text"> {{ Setting::footer() }} </p>
			</footer>
		</div>

		<!-- JS -->
		{{ HTML::script('themes/flat/js/jquery-1.8.3.min.js') }}
		{{ HTML::script('themes/flat/js/bootstrap.min.js') }}
		{{ HTML::script('themes/flat/js/application.js') }}
		@yield('scripts')
	</body>
</html>