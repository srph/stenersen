@if(Session::has('success'))
	@include('template/modules/alert.success')
@elseif(Session::has('error'))
	@include('template/modules/alert.error')
@endif