<?php

class Setting extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'settings';

	/**
	 * Columns fillable by the model
	 *
	 * @var array
	 */
	protected $fillable = array(
		'name',
		'value'
	);

	/**
	 * Checks if this model uses timestamps
	 *
	 * @var boolean
	 */
	public $timestamps = false;

	/**
	 * Get requested setting
	 *
	 * @return 	Setting
	 */
	public static function getRequested($name)
	{
		$setting = self::where('name', $name)
			->first();

		return $setting;
	}

	/**
	 * Get set email
	 *
	 * @return 	Setting
	 */
	public static function email()
	{
		$setting = self::getRequested('email')
			->value;

		return $setting;
	}

	/**
	 * Get set title
	 *
	 * @return 	Setting
	 */
	public static function title()
	{
		$setting = self::getRequested('title')
			->value;

		return $setting;
	}

	/**
	 * Get set footer note
	 *
	 * @return 	Setting
	 */
	public static function footer()
	{
		$setting = self::getRequested('footer')
			->value;

		return $setting;
	}

	/**
	 * Get set slogan
	 *
	 * @return 	Setting
	 */
	public static function slogan()
	{
		$setting = self::getRequested('slogan')
			->value;

		return $setting;
	}

	/**
	 * Check if site is live
	 *
	 * @return 	Setting
	 */
	public static function liveSite()
	{
		$setting = self::getRequested('liveSite')
			->value;

		return $setting;
	}

	/**
	 * Front description
	 *
	 * @return 	Setting
	 */
	public static function frontDescription()
	{
		$setting = self::getRequested('frontDescription')
			->value;

		return $setting;
	}

	/**
	 * Adds a checked indicator to the checkbox
	 *
	 * @param 	string 	$property
	 * @return 	string
	 */
	public static function checked($property)
	{
		$setting = self::getRequested($property);
		
		return ($setting->value == 1)
			? 'checked'
			: '';
	}

}