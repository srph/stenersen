<?php

class Page extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'pages';

	/**
	 * Columns fillable by the model
	 *
	 * @var array
	 */
	protected $fillable = array(
		'parent_id',
		'title',
		'body',
		'slug',
		'live',
		'inNavigation',
		'isHome',
		'order',
		'metaDescription'
	);

	/**
	 * Checks whether the model timestamps
	 *
	 * @return boolean
	 */
	public $timestamps = true;

	/**
	 * Returns appropriate string if the page
	 * is either live or Neit
	 *
	 * @return 	string
	 */
	public function live()
	{
		return ($this->live)
		 ? 'Leve'
		 : 'Utkast';
	}

	/**
	 * Returns appropriate string if the page
	 * is in the navigation or Neit
	 *
	 * @return 	string
	 */
	public function inNavigation()
	{
		return ($this->inNavigation)
			? 'Ja'
			: 'Nei';
	}

	/**
	 * Returns appropriate string if the page
	 * is the default homepage
	 *
	 * @return 	string
	 */
	public function isHomepage()
	{
		return ($this->isHome)
			? 'Standard Hjemmeside'
			: '';
	}

	/**
	 * Adds a checked indicator to the checkbox
	 *
	 * @return 	string
	 */
	public function checked($property)
	{
		return ($this->$property)
			? 'checked'
			: '';
	}

	/**
	 * Returns link of the page
	 *
	 * @return 	string
	 */
	public function getLink()
	{
		if($this->isHome)
			return url();
		
		$link = url() .
			'/' .
			$this->slug;

		return $link;
	}

	/**
	 * Returns an indicator if the given number
	 * matches the position of the page
	 *
	 * @return 	string
	 */
	public function checkCurrentOrder($index)
	{
		return ($this->order == $index)
			? '(Current)'
			: '';
	}

	/**
	 * Adds a selected indicator to the dropdown-menu
	 *
	 * @return 	string
	 */
	public function selected($index)
	{
		return ($this->order == $index)
			? 'selected="selectbox"'
			: '';
	}

	/**
	 * This returns pages that satisfies
	 * the following conditions:
	 * - In navigation
	 * - Is live
	 * and orders it depending on its arrangement
	 *
	 * @return 	Page
	 */
	public static function getLive()
	{
		$page = self::where('live', true)
			->where('inNavigation', true)
			->orderBy('order', 'asc');

		return $page;
	}

	public function isReady()
	{
		return ($this->live == true &&
			$this->inNavigation == true)
			? true
			: false;
	}

	/**
	 * Returns the set homepage
	 *
	 * @return 	Page
	 */
	public static function getHomepage()
	{
		$page = self::where('isHome', true)
			->first();

		return $page;
	}

	/**
	 * Fetch the requested page with the
	 * specified slug
	 *
	 * @return 	Page
	 */
	public static function getFromSlug($slug = null)
	{
		if(!is_null($slug)) {
			$page = self::getLive()
				->where('slug', $slug)
				->first();

			return $page;			
		}
		return;
	}

	/**
	 * Set a page to homepage
	 *
	 * 
	 * @param 	Page 	$page
	 * @return  boolean
	 */
	public static function setHome(Page $page)
	{
		if(!$page->isHome) {
			$home = self::where('isHome', true)
				->first();
			if(!empty($home)) {
				$home->isHome = false;
				$home->save();
			}

			$page->isHome = true;
			$page->save();
		}
	}

	/**
	 * Sort order
	 *
	 * @param 	int 	$newOrder
	 * @param 	Page 	$original
	 * @return 	void
	 */
	public static function setOrder($newOrder, Page $original)
	{
		$page = self::where('order', $newOrder)
			->first();

		// If a page in specified order does Neit exist, simply return
		if(!empty($page)) {
			// Get the page of last order
			$next = self::where('order', $newOrder + 1)
				->first();
			$last = self::getLastOrder();

			// If there are pages in position which
			// the order requested
			if($newOrder <= $last->order) {

				// If the original order was in a lower position
				// than the page existing the specified order
				//
				if($original->order > $newOrder) {
					$var = $newOrder + 1;
				} else {
					// Otherwise, simply swap
					$var = $original->order;
				}

				// 2 - 4 == -2
				// 2 - 3 == -1
				if(($original->order - $newOrder) > 1) {
					// Grab all pages that is equal to 
					// or greater than the requested order
					$replaceSet = self::where('order', '>', $newOrder)
						->get();

					foreach($replaceSet as $set) {
						// Increments its positions by 1
						$set->order = $set->order + 1;
						$set->save();
					}
				}

				$page->order = $var;
				$page->save();


				
			}

			// Position checking
			// If in case a certain position is Neit being used
		}

		return $newOrder;
	}

	/**
	 * Get the last arranged in ascending order
	 *
	 * @return 	Page
	 */
	public static function getLastOrder()
	{
		$page = self::orderBy('order', 'desc')
			->first();

		return $page;
	}

	/**
	 * Get the last stored page
	 *
	 * @return 	Page
	 */
	public static function getLastRecord()
	{
		$page = self::orderBy('order', 'desc')
			->first();

		return $page;
	}

	public function loadModule()
	{
		$dom = new domDocument;
		// $dom->loadHTML($this->convertedModules());
		$dom->loadHTML(stripslashes('<div> <a href="> </div>'));
		$dom->preserveWhiteSpace = false;
		return $dom->saveHTML();
	}

	/**
	 * Turns a string to a slug
	 *
	 * @param 	string 	$string
	 * @return 	string
	 */
	public static function slugify($string, $delimiter = "-")
	{
		$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $string);
		$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $string);
		$clean = strtolower(trim($clean, '-'));
		$clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

		return $clean;
	}

	/**
	 *
	 * Generate the form
	 */
	public function convertedModules()
	{
		$form = '<form id="custom-form" class="custom-form" action="mail/us" method="POST">' .
			// '{{ Form::token() }}' . 
			'<div id="email" class="form-group">' .
				"<label> Email </label>" .
				'<input type="email" name="email" class="form-control">' .
			'</div>' .

			'<div id="full_name" class="form-group">' .
				'<label> Full Name </label>' .
				'<input type="text" name="full_name" class="form-control">' .
			'</div>' .

			'<div id="subject" class="form-group">' .
				'<label> Subject </label>' .
				'<input type="text" name="subject" class="form-control">' .
			'</div>' .

			'<div id="message" class="form-group">' .
				'<label> Message: </label>' .
				'<textarea name="message" class="form-control" rows="12"></textarea>' .
			'</div>' .

			'<div class="form-group">' .
				'<button id="submit-button" type="submit" class="btn btn-primary btn-block">Send Email</button>' .
			'</div>' .
		'</form>';

		$module = preg_replace('/\[contactForm\]/', $form, $this->body);
		return $module;
	}

	/**
	 * Decrement all pages that are on a higher position
	 * than the specified Page
	 *
	 * @param 	Page 	$page
	 * @return 	void
	 */
	public static function decrementOrderOnDelete(Page $page)
	{
		$order = $page->order;
		$set = Page::where('order', '>', $order)
			->get();
		if(!empty($set)) {
			foreach($set as $data) {
				$data->order = $data->order - 1;
				$data->save();
			}
		}

	}

	/**
	 * Select default homepage in case homepage was deleted
	 *
	 * @return 	void
	 */
	public static function selectDefaultHome()
	{
		$page = Page::getLive()
			->first();

		$page->isHome = true;
		$page->save();
	}

	/**
	 * Returns the appropriate meta title
	 *
	 * @return 	string
	 */
	public function metaTitle()
	{
		if(is_null($this->title) ||
			empty($this->title)) {
			$title = ' ';
		} else {
			$title = Setting::title() .
				' - ' .
				$this->title;
		}

		return $title;
	}

	/**
	 * Returns meta description
	 *
	 * @return 	string
	 */
	public function metaDescription()
	{

		if(is_null($this->metaDescription) ||
			empty($this->metaDescription)) {
			$set = $this->metaDescription;
		}

		if(empty($set) || is_null($set)) {
			$set = ' ';
		}

		return $set;
	}

}