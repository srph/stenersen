<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * Checks whether the model timestamps
	 *
	 * @return boolean
	 */
	public $timestamps = true;	

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

	/**
	 * Validate input
	 *
	 * @param 	array 	$input
	 * @param 	string 	$type
	 * @return 	Validator
	 */
	public static function validate($input, $type = 1)
	{
		$rules = [];
		switch($type) {
			case 1:
				$rules = array(
					'email'					=>	"required|email",
					'password'				=>	'required|alpha_dash|min:5|confirmed',
					'password_confirmation'	=>	'required|alpha_dash|min:5',
					'first_name'			=>	'alpha_num',
					'last_name'				=>	'alpha_num'
				);
				break;
			case 2:
				$rules = array(
					'password'				=>	'required|alpha_dash|min:5|confirmed',
					'password_confirmation'	=>	'required|alpha_dash|min:5',
					'first_name'			=>	'alpha_num',
					'last_name'				=>	'alpha_num'
				);
				break;
			default:
				return;
				break;
		}

		return Validator::make($input, $rules);
	}

	/**
	 * Returns an indicator for checkboxes
	 * that the user is activated
	 *
	 * @param 	User 	$user
	 * @return 	string
	 */
	public static function activatedCheckbox($user)
	{
		return ($user->activated)
			? 'checked'
			: '';
	}

	/**
	 * Returns a string that the user is activated
	 *
	 * @param 	User 	$user
	 * @return 	string
	 */
	public static function activatedString($user)
	{
		return ($user->activated)
			? 'Aktivert'
			: '';
	}

}