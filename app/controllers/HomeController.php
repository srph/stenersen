<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	/**
	 * Redirects to the set homepage
	 *
	 * @return 	Response
	 */
	public function getIndex()
	{
		if(Setting::liveSite() != 0) {
			$page = Page::getHomepage();
			if(empty($page)) {
			 	$page = Page::getLive()
			 		->first();
			 	if(!empty($page)) {
			 		return Redirect::to($page->slug);
			 	} else {
			 		App::abort('404', 'No homepage assigned');
			 	}
			 }

			$pages = Page::getLive()->get();

			return View::make('index')
				->with('main', $page)
				->with('pages', $pages);
		}

		return View::make('template.coming_soon');
	}

	/**
	 * Shows page of requested slug
	 *
	 * @return Response
	 */
	public function getPage($slug)
	{
		if(Setting::liveSite() != 0) {
			$main = Page::getFromSlug($slug);
			if(empty($main) || !$main->isReady())
				App::abort('404', 'Requested page does not exist');

			$pages = Page::getLive()->get();

			return View::make('index')
				->with('main', $main)
				->with('pages', $pages);
		}

		return View::make('template.coming_soon');
	}

	public function getSandbox($slug)
	{
		if(Setting::liveSite() != 0) {
			$main = Page::getFromSlug($slug);
			if(empty($main) || !$main->isReady())
				App::abort('404', 'Requested page does not exist');

			$pages = Page::getLive()->get();

			return View::make('sandbox')
				->with('main', $main)
				->with('pages', $pages);
		}

		return View::make('template.coming_soon');
	}

}