<?php

class DashboardController extends BaseController {

	/**
	 *
	 *
	 */
	public function __construct()
	{		
		$this->beforeFilter('auth', array('only' => array('getIndex')));
	}

	/**
	 * Display the dashboard
	 *
	 * @return 	Response
	 */
	public function getIndex()
	{
		return View::make('dashboard.index');
	}
}