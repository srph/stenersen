<?php

class FileController extends BaseController {

	/**
	 *
	 *
	 */
	public function __construct()
	{
		$this->beforeFilter('auth', array('only' => array('getIndex')));
	}

	/**
	 *
	 *
	 */
	public function getIndex()
	{
		return View::make('dashboard/file.index');
	}
}