<?php

class PageController extends BaseController {

	/**
	 *
	 *
	 *
	 */
	public function __construct()
	{
		$this->beforeFilter('auth', array('only' => array(
			'index', 'create', 'store', 'show',
			'edit', 'update', 'delete'
		)));
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$pages = Page::all();
		return View::make('dashboard/page.index')
			->with('pages', $pages);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('dashboard/page.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$errors = [];
		// $parent = Page::setParent(Input::get('parent'));
		$order = Page::getLastOrder()->order + 1;
		$inNavigation = (Input::get('inNavigation') == "true")
			? true
			: false;

		$live = (Input::get('live') == "true")
			? true
			: false;

		$title = Input::get('title');
		$slug = (!Input::has('slug'))
			? Page::slugify($title)
			: Input::get('slug');
		
		$slug_exists = Page::where('slug', '=', $slug)
			->get();

		if($slug == 'dashboard') {
			$errors['slug'] = 'Permalink brukte søkeordet: [dashbord]';
		} elseif(!is_null($slug_exists) &&
			!empty($slug_exists) &&
			count($slug_exists) >= 1) {
			$errors['slug'] = 'Innsatt slug finnes allerede';
		}

		$parent = 0;
		$body = Input::get('body');

		$metaDescription = Input::get('metaDescription');

		$page = new Page(array(
			'parent_id'			=> 	$parent,
			'title'				=>	$title,
			'body'				=>	$body,
			'slug'				=>	$slug,
			'live'				=>	$live,
			'inNavigation'		=>	$inNavigation,
			'isHome'			=>	false,
			'order'				=>	$order,
			'metaDescription'	=>	$metaDescription
		));

		if(count($errors) === 0) {
			if($page->save()) {
				$success = $page->title .
					' har blitt opprettet!';
				Session::flash('success', $success);
				return Response::json(array(
					'status'	=>	true
				));
			}
		}

		$error = 'Det har oppstått en feil';
		return Response::json(array(
			'status'	=>	false,
			'error'		=>	$error,
			'errors'	=>	$errors
		));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$page = Page::find($id);
		if($page->live)
			return Redirect::to('/' . $page->slug);

		return Redirect::to('/');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$page = Page::find($id);
		
		return View::make('dashboard/page.edit')
			->with('page', $page);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$errors = [];
		$page = Page::find($id);
		$navigation = (Input::get('inNavigation') == "true")
			? true
			: false;

		$live = (Input::get('live') == "true")
			? true
			: false;


		// Process appropriate slug
		$title = Input::get('title');
		$slug = (!Input::has('slug'))
			? Page::slugify($title)
			: Input::get('slug');

		if($slug == 'dashboard') {
			$errors['slug'] = 'Permalink brukte søkeordet: [dashbord]';
		} elseif($slug !== $page->slug) {
			$slug_exists = Page::where('slug', $slug)
				->get();
				
			if(!is_null($slug_exists) &&
				!empty($slug_exists) &&
				count($slug_exists) >= 1) {
				$errors['slug'] = 'Innsatt slug finnes allerede';
			}
		}

		// Process homepage
		$home = (Input::get('isHome') == "true")
			? true
			: false;
		$sg = Input::get('isHome');
		// return Response::json(array($sg, $home));
		if(!$page->isHome && $home) {
			Page::setHome($page);
		}

		// Process page order
		$order = Input::get('order');
		if($page->order !== $order) {
			$page->order = Page::setOrder($order, $page);
		}

		$body = Input::get('body');

		$metaDescription = Input::get('metaDescription');

		// [] Parent
		$page->title				=	$title;
		$page->body					=	$body;
		$page->slug					=	$slug;
		$page->live					=	$live;
		$page->inNavigation			=	$navigation;
		$page->order				=	$order;
		$page->metaDescription		=	$metaDescription;
		if(count($errors) == 0) {
			if($page->save()) {
				$success = $page->title .
					' har blitt vellykket oppdatert!';
				Session::flash('success', $success);
				return Response::json(array(
					'status'	=>	true
				));
			}
		}

		$error = 'Det har oppstått en feil';
		return Response::json(array(
			'status'	=>	false,
			'error'		=>	$error,
			'errors'	=>	$errors
		));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$page = Page::find($id);

		$isHome = ($page->isHome)
			? true
			: false;

		$status = ($page->delete())
			? true
			: false;

		Page::decrementOrderOnDelete($page);
		if($isHome) Page::selectDefaultHome();

		if($status) {		
			$success = $page->title . ' page has been deleted';
			Session::flash('success', $success);
		} else {
			$error = 'An error has occured while deleting ' . $page->title;
			Session::flash('error', $error);
		}

		return Response::json(array(
				'status'	=>	$status
		));
	}

}