<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$users = Sentry::findAllUsers();

		return View::make('dashboard/user.index')
			->with('users', $users);;
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{	

		return View::make('dashboard/user.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validation = User::validate(Input::all());
		if($validation->passes()) {

			$activated = (Input::get('activated') == "true")
				? true
				: false;

			$user = array(
				'email'			=>	Input::get('email'),
				'password'		=>	Input::get('password'),
				'activated'		=>	$activated,
				'first_name'	=>	Input::get('first_name'),
				'last_name'		=>	Input::get('last_name')
			);

			if(Sentry::register($user)) {
				Session::flash('success', 'En ny bruker er lagt til!');
				return Response::json(array(
					'status'	=>	true
				));
			}
		}
		
		// return Response::json($validation->messages());
		$error = 'Det har oppstått en feil under oppretting av brukeren';
		return Response::json(array(
			'status'	=>	false,
			'error'		=>	$error,
			'errors'	=>	$validation->messages()->all()
		));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = Sentry::findUserById($id);

		return View::make('dashboard/user.edit')
			->with('user', $user);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$errors = [];
		$user = Sentry::findUserById($id);
		$validation = User::validate(Input::all(), 2);

		$activated = (Input::get('activated') == "true")
			? true
			: false;
		if($validation->passes()) {
			// $user->email		=	Input::get('email');
			$user->password		=	Input::get('password');
			$user->activated	=	$activated;
			$user->first_name	=	Input::get('first_name');
			$user->last_name	=	Input::get('last_name');

			if($user->save()) {
				Session::flash('success', 'En ny bruker er lagt!');
				return Response::json(array(
					'status'	=>	true
				));
			}
		}

		$error = 'Det har oppstått en feil under oppretting av brukeren';
		return Response::json(array(
			'status'	=>	false,
			'error'		=>	$error,
			'errors'	=>	$errors
		));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$user = Sentry::findUserById($id);
		$status = ($user->delete())
			? true
			: false;		

		if($status) {
			Session::flash('success', 'Brukeren er blitt slettet');
		} else {
			Session::flash('error', 'Det har oppstått en feil under sletting av bruker');
		}
		return Response::json(array(
			'status'	=>	$status
		));
	}

}