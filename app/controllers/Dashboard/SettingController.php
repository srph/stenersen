<?php

class SettingController extends BaseController {

	/**
	 *
	 *
	 */
	public function __construct()
	{		
		$this->beforeFilter('auth', array('only' => array('getIndex', 'putIndex')));
	}

	/**
	 * Display system settings
	 *
	 * @return 	Response
	 */
	public function getIndex()
	{
		return View::make('dashboard/settings.index');
	}

	/**
	 * Update system settings
	 *
	 * @return 	Response
	 */
	public function putIndex()
	{
		$title = Setting::getRequested('title');
		$slogan = Setting::getRequested('slogan');
		$email = Setting::getRequested('email');
		$footer = Setting::getRequested('footer');
		$liveSite = Setting::getRequested('liveSite');
		$frontDescription = Setting::getRequested('frontDescription');

		$isLive = (Input::has('liveSite'))
			? '1'
			: '0';

		$title->value = Input::get('title');
		$slogan->value = Input::get('slogan');
		$email->value = Input::get('email');
		$footer->value = Input::get('footer');
		$liveSite->value = $isLive;
		$frontDescription->value = Input::get('frontDescription');
		if($title->save() &&
			$slogan->save() &&
			$email->save() &&
			$footer->save() &&
			$liveSite->save() &&
			$frontDescription->save()) {
			Session::flash('success', 'Konfigurasjoner har blitt oppdatert');
			return Redirect::to('dashboard/settings');
		}
	}

}