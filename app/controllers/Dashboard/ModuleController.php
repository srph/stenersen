<?php

class ModuleController extends BaseController {

	/**
	 * Process the mailing
	 *
	 *
	 */
	public function postSendMail()
	{
		$errors = [];

		Validator::extend('alpha_space', function($attr, $value) {
    		return preg_match('/^([a-z\x20])+$/i', $value);
		});
		$rules = array(
			'email'			=>	'required|email',
			'subject'		=>	'required|between:10,64',
			'full_name'		=>	'required|alpha_space',
			'message'		=>	'required|min:25'
		);

		$validation = Validator::make(Input::all(), $rules);

		if($validation->passes()) {
			$msg = Input::get('message');

			$data = [
				'msg'	=>	$msg
			];

			$to = Setting::email();
			$subject = Input::get('subject');
			$from = Input::get('email');
			$name = Input::get('full_name');

			Mail::send('template.email', $data, function($message) use($to, $from, $subject, $name)
			{
				$message->from($from, $name)
				->to($to)
				->subject($subject);
			});

			Session::flash('success', 'Mail has been sent');
			return Response::json(array(
				'status'	=>	true
			));
		}

		$messages = $validation->messages();
		$errors['full_name']	= $messages->first('full_name');
		$errors['email']		= $messages->first('email');
		$errors['message']		= $messages->first('message');
		$errors['subject']		= $messages->first('subject');

		return Response::json(array(
			'status'	=>	false,
			'error'		=>	'An error has occured',
			'errors'	=>	$errors
		));

	}
}