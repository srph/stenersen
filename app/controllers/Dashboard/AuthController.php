<?php

/**
 * Controller which handles the authentication
 * of users and administrators
 *
 */
class AuthController extends BaseController {

	/**
	 *
	 *
	 */
	public function __construct()
	{
		$this->beforeFilter('csrf', array('only' => array('postLogin')));
		$this->beforeFilter('guest', array('except' => array('getLogout')));
		$this->beforeFilter('auth', array('only' => array('getLogout')));
	}

	/**
	 *
	 *
	 */
	public function getIndex()
	{
		return View::make('dashboard/auth.blank');
	}

	/**
	 * Login the authenticated user
	 *
	 * @return 	Response
	 */
	public function postLogin()
	{
		try {
			// Set login credentials
			$credentials = array(
				'email'		=>	Input::get('email'),
				'password'	=>	Input::get('password')
			);

			// Try to authenticate the user
			$user = Sentry::authenticate($credentials, false);
		} catch (Cartalyst\Sentry\Users\LoginRequiredException $e) {
			$message = 'Pålogging feltet er påkrevd.';
		} catch (Cartalyst\Sentry\Users\PasswordRequiredException $e) {
			$message = 'Passord feltet er påkrevd.';
		} catch (Cartalyst\Sentry\Users\WrongPasswordException $e) {
			$message = 'Feil passord, prøv igjen.';
		} catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
			$message = 'Bruker ble ikke funnet.';
		} catch (Cartalyst\Sentry\Users\UserNotActivatedException $e) {
			$message = 'Brukeren er ikke aktivert.';
		}

		if(empty($message)) {
			Session::flash('success', 'Du har logget inn');
			return Redirect::to('dashboard');
		}

		Session::flash('error', $message);
		return Redirect::to('dashboard/auth');
	}

	/**
	 * Logout the authenticated user
	 *
	 * @return 	Response
	 */
	public function getLogout()
	{
		Sentry::logout();
		Session::flash('success', 'Du har klart logget ut!');
		return Redirect::to('dashboard/auth');
	}
}