<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Upload dir
    |--------------------------------------------------------------------------
    |
    | The dir where to store the images (relative from public)
    |
    */

    'dir' => 'files',

    /*
    |--------------------------------------------------------------------------
    | Access filter
    |--------------------------------------------------------------------------
    |
    | Filter callback to check the files
    |
    */

    'access' => 'Barryvdh\Elfinder\Elfinder::checkAccess',

    /*
    |--------------------------------------------------------------------------
    | Roots
    |--------------------------------------------------------------------------
    |
    | By default, the roots file is LocalFileSystem, with the above public dir.
    | If you want custom options, you can set your own roots below.
    |
    */

    'roots' => array(
        array(
            'driver' => 'LocalFileSystem',
            'path'   => public_path() . '/files/',
            'URL'    => url('files'),
            'uploadDeny' => array(
                'text/php',
                'text/x-php',
                'application/php',
                'application/x-php',
                'application/x-httpd-php',
                'application/x-httpd-php-source'
            )
        )
    ),
);
