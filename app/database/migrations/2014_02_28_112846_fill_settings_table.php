<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FillSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		$db = DB::table('settings');
		
		$settings = array(
			array(
				'id'		=>	1,
				'name'		=>	'title',
				'value'		=>	'Stenersen Co.'
			),

			array(
				'id'		=>	2,
				'name'		=>	'slogan',
				'value'		=>	'Grafiski Industri'
			),

			array(
				'id'		=>	3,
				'name'		=>	'footer',
				'value'		=>	'Besøksadr: Sandakerveien 10 A, P.B. 4380 Nydalen, 0402 Oslo, Tlf: 22 37 67 58 - Fax: 22 37 40 31, e-post: post@stenersen.no'
			),

			array(
				'id'		=>	4,
				'name'		=>	'email',
				'value'		=>	'post@stenersen.no'
			),

			array(
				'id'		=>	5,
				'name'		=>	'liveSite',
				'value'		=>	1
			),

			array(
				'id'		=>	6,
				'name'		=>	'frontDescription',
				'value'		=>	'Stenersen Co. is a cabin association in Frogn municipality where all operations are based on voluntary efforts.'
			)
		);

		$db->insert($settings);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		$db = DB::table('settings');
		$db->delete();
	}

}
