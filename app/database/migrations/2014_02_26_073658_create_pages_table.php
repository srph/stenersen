<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pages', function($table)
		{
			$table->increments('id');
			$table->integer('parent_id')->nullable();
			$table->string('title')->nullable();
			$table->text('body')->nullable();
			$table->string('slug');
			$table->boolean('live');
			$table->boolean('isHome');
			$table->boolean('inNavigation');
			$table->integer('order');
			$table->string('metaDescription')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pages');
	}

}
