<?php

class UserSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$db = DB::table('users');
		$db->delete();

		$users = array(
			array(
				'id'			=>	1,
				'email'			=>	'admin@admin.com',
				'password'		=>	'admin',
				'activated'		=>	1,
				'first_name'	=>	'Admin',
				'last_name'		=>	'Admin'
			),

			array(
				'id'			=>	2,
				'email'			=>	'admin2@admin.com',
				'password'		=>	'admin',
				'activated'		=>	1,
				'first_name'	=>	'Admin',
				'last_name'		=>	'Admin'
			),
		);

		foreach($users as $user) {
			Sentry::register($user);
		}
	}

}