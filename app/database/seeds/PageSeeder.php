<?php

class PageSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$db = DB::table('pages');
		$db->delete();

		$pages = array(
			array(
				'id'				=>	1,
				'parent_id'			=>	0,
				'title'				=>	'Home',
				'body'				=>	'Lipsum yeah',
				'slug'				=>	'index',
				'live'				=>	true,
				'isHome'			=>	true,
				'inNavigation'		=>	true,
				'order'				=>	1
			),

			array(
				'id'				=>	2,
				'parent_id'			=>	0,
				'title'				=>	'About Me',
				'body'				=>	'Lipsum yeah',
				'slug'				=>	'about-me',
				'live'				=>	true,
				'isHome'			=>	false,
				'inNavigation'		=>	true,
				'order'				=>	2
			),

			array(
				'id'				=>	3,
				'parent_id'			=>	0,
				'title'				=>	'Contact',
				'body'				=>	'Lipsum yeah',
				'slug'				=>	'contact',
				'live'				=>	true,
				'isHome'			=>	false,
				'inNavigation'		=>	true,
				'order'				=>	3
			),
		);

		$db->insert($pages);
	}

}