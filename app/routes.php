<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(array('before' => 'auth'), function()
{
	\Route::get('elfinder', 'Barryvdh\Elfinder\ElfinderController@showIndex');
	\Route::any('elfinder/connector', 'Barryvdh\Elfinder\ElfinderController@showConnector');
});

Route::group(array('prefix' => 'dashboard'), function()
{
	/**
	 *
	 *
	 */
	Route::get('sandbox/{slug}', 'HomeController@getSandbox');
	
	/**
	 * @link dashboard/account
	 * Accounts management routes
	 */
	Route::resource('account', 'UserController');

	/**
	 * @link dashboard/page/*
	 * Page routes
	 */
	Route::resource('page', 'PageController');

	/**
	 * @link dashboard/file/
	 * File management API route
	 */
	Route::controller('file', 'FileController');

	/**
	 * @link dashboard/auth/*
	 * Authentication routes
	 */
	Route::controller('auth', 'AuthController');

	/**
	 * @link system
	 * System settings routes
	 */
	Route::controller('settings', 'SettingController');

	/**
	 * @link dashboard
	 * Dashboard routes
	 */
	Route::controller('/', 'DashboardController');
});

/**
 * @link /mail/us
 * Module (mail)
 */ 
Route::post('mail/us', 'ModuleController@postSendMail');

/**
 * @link /*(slug)
 * Live pages
 */
Route::get('/', 'HomeController@getIndex');
Route::get('{slug}', 'HomeController@getPage');